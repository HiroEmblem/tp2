﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tp1_Dev_Appli
{
    /// <summary>
    /// Logique d'interaction pour Employe.xaml
    /// </summary>
    public partial class EmployeList : UserControl
    {
        public EmployeList()
        {
            InitializeComponent();
        }

        private void Btn_Ajout_Click(object sender, RoutedEventArgs e)
        {   NavigationViewModel Go_Ajout = new NavigationViewModel();
            Application.Current.MainWindow.DataContext = Go_Ajout; 
            Go_Ajout.SelectedViewModel = new AjouterEmployeViewModel();
        }

        private void Btn_Modif_Click(object sender, RoutedEventArgs e)
        {
            NavigationViewModel Go_Modif = new NavigationViewModel();
            Application.Current.MainWindow.DataContext = Go_Modif;
            Go_Modif.SelectedViewModel = new ModifierEmployeViewModel();
        }

        private void Btn_Supp_Click(object sender, RoutedEventArgs e)
        {
            NavigationViewModel Go_Supp = new NavigationViewModel();
            Application.Current.MainWindow.DataContext = Go_Supp;
            Go_Supp.SelectedViewModel = new SupprimerEmployeViewModel();
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
